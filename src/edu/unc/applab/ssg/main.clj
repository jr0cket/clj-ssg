(ns edu.unc.applab.ssg.main
  "This is the top-level namespace and the main entry point for the static site
  generator. Specifically, the -main function in this namespace is called when
  this project is executed, e.g. with `clj -m edu.unc.applab.ssg.main`.

  Note that this project follows the 'functional core, imperative shell' idea
  outlined in Gary Bernhardt's talk _Boundaries_:
  https://www.destroyallsoftware.com/talks/boundaries. Within that architecture,
  this namespace is exactly the imperative shell: no more and no less. In other
  words, this is the only namespace in the project with any effects. Every other
  namespace contains only pure functions without effects."

  (:require [clojure.edn :as edn]
            [clojure.java.io :as io]
            [clojure.java.shell :as shell]
            [clojure.string :as string]
            [edu.unc.applab.ssg.config :as config]
            [edu.unc.applab.ssg.parse :as parse]
            [edu.unc.applab.ssg.site :as site]
            [edu.unc.applab.ssg.util :as util :refer [conj-many-if]])
  (:import [java.io FileNotFoundException IOException]))

(defn die
  "Print the given message to stderr and quit with a non-zero exit code."
  [message]
  (binding [*out* *err*]
    (println message))
  (System/exit 1))

(defn assert-pandoc
  "Check for the existence of a pandoc executable, and exit with a suitably
  explanatory message if one is not found."
  []
  (try
    (shell/sh "pandoc" "--version")
    (catch IOException e
      (die
        (str "Pandoc was not found.\n"
             "You must install it to use this static site generator.\n"
             "See http://pandoc.org/installing.html for details.\n\n"
             "Specifically, trying to shell out to `pandoc --version`\n"
             "threw the following exception:\n\n"
             (prn-str e))))))

(defn assert-site-structure
  "Check that the files and directories needed by the SSG are valid. If not,
  print a suitably explanatory message and exit."
  []
  (letfn [(die-missing-file-or-dir [obj-type obj-name]
            (die
              (format
                (str "The '%s' %s needed by this static site generator (SSG)"
                     " was not found.\n\n"
                     "Possible causes:\n"
                     "- You're running the SSG from the wrong directory.\n"
                     "- You haven't created all the files needed by the SSG.\n\n"
                     "You can find more info in the README for the SSG:\n"
                     "https://gitlab.com/unc-app-lab/clj-ssg/")
                obj-name obj-type)))

          (check-dir [dir]
            (let [obj (io/file dir)]
              (when-not (and (.exists obj)
                             (.canRead obj)
                             (.isDirectory obj))
                (die-missing-file-or-dir "directory" dir))))

          (check-file [file]
            (let [obj (io/file file)]
              (when-not (and (.exists obj)
                             (.canRead obj)
                             (.isFile obj))
                (die-missing-file-or-dir "file" file))))

          (assert-files-in-dir [dir file-pred error-predicate-str]
            (when-not (every? file-pred (-> dir io/file .listFiles))
              (die
                (format "Error: the '%s' directory %s.\n"
                        dir error-predicate-str))))

          (check-nesting [dir]
            (assert-files-in-dir dir
                                 #(.isFile %)
                                 "has subdirectories inside it"))

          (check-readable-files [dir]
            (assert-files-in-dir dir
                                 #(.canRead %)
                                 "has unreadable items inside it"))]

    (mapv check-dir ["pages" "posts" "resources" "template"])
    (mapv check-file ["template/header.html" "template/footer.html"])
    (mapv check-nesting ["pages" "posts"])
    (mapv check-readable-files ["pages" "posts" "resources"])))

(defn has-metadata-keys?
  "Return whether the given contents of a file read from disk contain metadata
  with the given keys."
  [contents keys]
  (let [metadata (first (parse/split-metadata contents))]
    (every? #(contains? metadata %) keys)))

(defn read-config!
  "Either return the valid config map read from the file `ssg-config.edn` or die
  with a descriptive error message."
  []
  (let [config (try
                 (edn/read-string (slurp "ssg-config.edn"))
                 (catch FileNotFoundException e config/default)
                 (catch RuntimeException e config/default))
        config (merge config/default config)]
    (when (config/invalid-map? config)
      (die (str "The config map found in `ssg-config.edn` is invalid.\n"
                "Here's why:\n"
                (config/invalid-explanation config))))
    config))

(defn read-pages!
  "Return a vector of page content strings, one for each file in the `pages/`
  directory. Also verify that the read files have the required metadata keys
  for pages, and if not, print an error message and quit."
  []
  (vec
    (for [page-file (util/find-files "pages" ".md")]
      (let [contents (slurp page-file)]
        (when-not (has-metadata-keys? contents [:title :stem])
          (die (str "There was a problem with the metadata for page '" page-file
                    "'.\nEither it did not contain metadata,\n"
                    "or the metadata did not contain the required information.\n"
                    "All pages must include YAML metadata like so:\n"
                    "https://github.blog/2013-09-27-viewing-yaml-metadata"
                    "-in-your-documents/\n\n"
                    "Also, pages must include the following metadata keys:\n"
                    "- title: the page title, visible in the HTML page title\n"
                    "- stem: where the page is found in the site\n"
                    "  For example, a stem of 'about' means that the page will\n"
                    "  be found at 'http://$host/about/'.\n")))
        contents))))

(defn read-posts!
  "Return a vector of post content strings, one for each file in the `posts/`
  directory. Also verify that the read files have the required metadata keys
  for posts, and if not, print an error message and quit."
  [{:keys [enable-authors enable-tags] :as config}]
  (vec
    (for [post-file (util/find-files "posts" ".md")]
      (let [contents (slurp post-file)]
        (when-not (has-metadata-keys? contents
                                      (if enable-authors
                                        [:title :author :date]
                                        [:title :date]))
          (die (str "There was a problem with the metadata for post '" post-file
                    "'.\nEither it did not contain metadata,\n"
                    "or the metadata did not contain the required information.\n"
                    "All posts must include YAML metadata like so:\n"
                    "https://github.blog/2013-09-27-viewing-yaml-metadata"
                    "-in-your-documents/\n\n"
                    "Also, posts must include the following metadata keys:\n"
                    "- title: the post title, visible in the HTML page title\n"
                    "  and in blog/author/tag indexes\n"
                    (if-not enable-authors
                      ""
                      (str
                        "- author: the author of the post, needed for rendering"
                        " the post HTML\n"))
                    "- date: the date of the post, used in the post URL\n\n"
                    (if-not enable-tags
                      ""
                      "(We also recommend comma-separated tags metadata.)\n"))))
        contents))))

(defn clean!
  "Remove any files generated by the static site generator on a previous run."
  []
  (try
    (util/rmdir-r "dist")
    (catch IOException e
      ;; happens when dist/ directory does not exist
      nil)))

(defn save-pages!
  "Given a seq of Pages (see .pages namespace), spit the page to the
  corresponding file under `dist/`, creating the directory first if it doesn't
  already exist."
  [pages]
  (doseq [{:keys [href html]} pages]
    (let [out-dir (str "dist" href)]
      (util/mkdir out-dir)
      (spit (str out-dir "index.html") html))))

(defn -main
  "Given static site content in directories pages/, posts/, resources/, and
  template/ (see README.md for details), create a static site ready for
  deployment in the dist/ directory, removing any preexisting content first.
  If any requirement is not found, fail with a clear message before changing
  anything on disk."
  []
  (assert-pandoc)
  (assert-site-structure)
  (let [config (read-config!)
        pages (read-pages!)
        posts (read-posts! config)
        dirs ["blog" "posts"]
        dirs (conj-many-if (:enable-tags config) dirs ["tag" "tags"])
        dirs (conj-many-if (:enable-authors config) dirs ["author" "authors"])]
    (clean!)
    (doseq [dir dirs]
      (util/mkdir (str "dist/" dir)))
    (let [files (util/find-files "resources")]
      (doseq [dir (util/needed-dist-dirs files)]
        (util/mkdir dir))
      (doseq [file files]
        (io/copy (io/file file)
                 (io/file (string/replace file "resources/" "dist/")))))
    (save-pages! (site/create-site config pages posts))
    (System/exit 0)))
