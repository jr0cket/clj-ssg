(ns edu.unc.applab.ssg.markup
  "This namespace is responsible for creating and manipulating markup. The
  functions in this namespace are pure, although the header and footer vars
  depend on files in the template/ directory."
  (:require [clojure.string :as string]
            [edu.unc.applab.ssg.util :as util]
            [hiccup.core :refer [html]]))

(def header
  (memoize (fn [] (slurp "template/header.html"))))
(def footer
  (memoize (fn [] (slurp "template/footer.html"))))

(defn frame
  "Given a title and body HTML content, frame it with the header and footer.
  Return an HTML string suitable for the full HTML document."
  [title html]
  (let [header (string/replace (header) "__TITLE__" title)]
    (str header html (footer))))

(defn author-url
  "Given an author, return the URL for the author page."
  [author]
  (format "/author/%s/" (util/slugify author)))

(defn tag-url
  "Given a tag, return the URL for the tag page."
  [tag]
  (format "/tag/%s/" (util/slugify tag)))

(defn author-link
  "Given an author, return an HTML link to the author page."
  [author]
  (html [:a {:href (author-url author)}
         author]))

(defn tag-link
  "Given a tag, return an HTML link to the tag page."
  [tag]
  (html [:a {:href (tag-url tag)}
         tag]))

(defn grouped-titles-html
  "Given (1) a seq of post groups, each of which contains a group name and a seq
  of post maps, and (2) a function to transform the group name into an HTML
  link, return HTML content of linked post titles listed under linked group
  names."
  [grouped-posts group-name->link]
  (letfn [(titles-ul [posts]
            (for [post posts]
              [:li
               [:a {:href (:href post)}
                (:title post)]]))]
    (string/join "\n"
                 (for [[group-name posts] grouped-posts]
                   (html [:h1 (group-name->link group-name)]
                         [:ul (titles-ul posts)])))))

(defn post-summary
  "Given a post map, return an HTML string containing the linked post title and
  preview."
  [{:keys [enable-tags enable-authors] :as config} post]
  (let [{:keys [href title date author tags snippet]} post
        tag-html (fn [tag]
                   [:a {:href (tag-url tag)}
                    tag])
        tags-html (fn [] (mapv tag-html tags))]
    (html
      [:div.post-preview
       [:h3.title [:a {:href href} title]]
       [:span.date date]
       (when enable-authors
         [:span.author
          [:a {:href (author-url author)} author]])
       (when enable-tags (apply conj [:div.tags] (tags-html)))
       [:span.snippet snippet]
       [:p [:a {:href href} "Read more&hellip;"]]])))

(defn posts-summary
  "Return HTML with a summary of all the given posts, in order."
  [config posts]
  (string/join "\n" (map (partial post-summary config) posts)))
