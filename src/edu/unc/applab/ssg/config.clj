(ns edu.unc.applab.ssg.config
  (:require [clojure.spec.alpha :as s]))

(s/def ::site-name (s/and string? (complement empty?)))

(s/def ::config-map (s/keys :req-un [::site-name]))

(def default
  {:site-name "Default site name"
   :enable-tags true
   :enable-authors true})

(defn invalid-map?
  [config]
  (not (s/valid? ::config-map config)))

(defn invalid-explanation
  [config]
  (s/explain-str ::config-map config))
