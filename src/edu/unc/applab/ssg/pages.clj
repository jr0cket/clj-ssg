(ns edu.unc.applab.ssg.pages
  "This namespace is responsible for creating Pages in the generated static
  site. A Page in this namespace means a tuple of href and HTML content."
  (:require
   [clojure.string :as string]
   [edu.unc.applab.ssg.markup :as markup]
   [edu.unc.applab.ssg.parse :as parse]
   [hiccup.core :refer [html]]))

(defrecord Page [href html])

(defn build-blog-index
  "Given all the post maps, return a Page for the blog index."
  [config posts]
  [(->Page "/blog/"
           (markup/frame "UNC App Lab Blog"
                         (->> posts
                              (map (partial markup/post-summary config))
                              (string/join "\n"))))])

(defn author-pages
  "Given posts grouped by author, return a map whose keys are author paths (e.g.
  '/author/jeff-terrell/') and whose values are the full Page for the author
  including summaries of all their posts."
  [config posts-by-author]
  (vec (for [[author posts] posts-by-author]
         (->Page
           (markup/author-url author)
           (markup/frame (str "Posts by " author)
                         (str (html [:h1 (str "Posts by " author)])
                              (markup/posts-summary config posts)))))))

(defn author-index
  "Given posts grouped by author, return the author index Page."
  [posts-by-author]
  (->Page
    "/authors/"
    (markup/frame
      "Author Index"
      (markup/grouped-titles-html (into (sorted-map) posts-by-author)
                                  markup/author-link))))

(defn build-author-pages
  "Given posts grouped by author, return all author-related Pages, including the
  author index."
  [config posts-by-author]
  (conj (author-pages config posts-by-author)
        (author-index posts-by-author)))

(defn tag-pages
  "Given posts grouped by tag, return a map whose keys are tag paths (e.g.
  '/tag/clojure/') and whose values are the full Page for the tag including
  summaries of all the corresponding posts."
  [config posts-by-tag]
  (vec (for [[tag posts] posts-by-tag]
         (->Page
           (markup/tag-url tag)
           (markup/frame (str "Posts tagged " tag)
                         (str (html [:h1 (str "Posts tagged " tag)])
                              (markup/posts-summary config posts)))))))

(defn tag-index
  "Given posts grouped by tag, return the tag index Page."
  [posts-by-tag]
  (->Page
    "/tags/"
    (markup/frame
      "Tag Index"
      (markup/grouped-titles-html (into (sorted-map) posts-by-tag)
                                  markup/tag-link))))

(defn build-tag-pages
  "Given posts grouped by tag, return all tag-related Pages, including the tag
  index."
  [config posts-by-tag]
  (conj (tag-pages config posts-by-tag)
        (tag-index posts-by-tag)))

(defn build-posts
  "Given post maps, return corresponding post Pages."
  [{:keys [enable-tags enable-authors] :as config} posts]
  (letfn [(href [post]
            (let [{:keys [slug date]} post
                  [year month day] (string/split date #"-")]
              (format "/posts/%s/%s/%s/%s/"
                      year month day slug)))
          (page [post]
            (let [{:keys [title date author tags body]} post]
              (markup/frame
                title
                (html
                  [:h1.title title]
                  [:div.metadata
                   [:span.date "Published " date]
                   (when enable-authors
                     [:span.author
                      "By "
                      [:a {:href (markup/author-url author)} author]])
                   (when enable-tags
                     (apply conj [:div.tags "Tagged: "]
                            (mapv (fn [tag]
                                    [:a {:href (markup/tag-url tag)} tag])
                                  tags)))]
                  body))))]
    (mapv #(->Page (href %) (page %)) posts)))

(defn build-pages
  "Given page content strings read from disk, return corresponding page Pages."
  [pages]
  (vec
    (for [page pages]
      (let [{:keys [title body href]} page]
        (->Page href (markup/frame title (html [:h1 title] body)))))))
