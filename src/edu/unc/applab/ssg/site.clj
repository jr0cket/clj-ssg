(ns edu.unc.applab.ssg.site
  "This namespace is responsible for creating the final site, which will be
  dumped to disk in the dist/ directory. All functions here are pure.

  As used here, a 'site' is a collection of (href, content) tuples."
  (:require [clojure.string :as string]
            [edu.unc.applab.ssg.markup :as markup]
            [edu.unc.applab.ssg.pages :as pages]
            [edu.unc.applab.ssg.parse :as parse]
            [edu.unc.applab.ssg.util :as util]
            [hiccup.core :refer [html]]))

(defn- add-recent-posts
  "Given post maps and a site value, create markup for the 'recent posts'
  section in the sidebar and replace the placeholder in each page with the
  created markup."
  [posts site]
  (let [recent-posts (take 3 posts)
        recent-posts-markup (html (apply conj [:ul]
                                         (map (fn [{:keys [title href]}]
                                                [:li [:a {:href href} title]])
                                              recent-posts)))]
    (vec
      (for [{:keys [href html]} site]
        (pages/->Page
          href
          (string/replace html "__RECENT_POSTS__" recent-posts-markup))))))

(defn- add-tags
  "Given post maps and a site value, create markup for the 'tags' section in the
  sidebar and replace the placeholder in each page with the created markup."
  [posts site]
  (let [tags (->> posts (mapcat :tags) (into (sorted-set)))
        tags-markup (html (apply conj [:ul]
                                 (map (fn [tag]
                                        [:li (markup/tag-link tag)])
                                      tags)))]
    (vec
      (for [{:keys [href html]} site]
        (pages/->Page href (string/replace html "__TAGS__" tags-markup))))))

(defn create-site
  "Given content strings read from disk for pages and posts, create a site,
  including the blog index, author pages, tag pages, and sidebar content for
  each page."
  [{:keys [enable-authors enable-tags] :as config} page-strings post-strings]
  (let [posts (parse/parse-posts config post-strings)
        pages (parse/parse-pages page-strings)
        site (concat
               (pages/build-pages pages)
               (pages/build-posts config posts)
               (pages/build-blog-index config posts)
               (when enable-authors
                 (->> posts
                      (group-by :author)
                      (pages/build-author-pages config)))
               (when enable-tags
                 (->> posts
                      (util/multi-group-by :tags)
                      (pages/build-tag-pages config))))
        add-tags-fn (if enable-tags (partial add-tags posts) identity)]
    (->> site
         (add-recent-posts posts)
         add-tags-fn)))
