(ns edu.unc.applab.ssg.util
  "A collection of general-purpose helper functions used elsewhere in the
  project."
  (:require [clojure.java.io :as io]
            [clojure.string :as string]))

(defn conj-many-if
  "If a predicate is true, return a collection with items conjoined; otherwise,
  return the given collection."
  [pred coll items]
  (if pred
    (apply conj coll items)
    coll))

(defn multi-group-by
  "Given a key and a sequence of maps, where the corresponding value in each map
  is a sequence, group the maps by the presence of individual items in the
  sequence.

  Example:
      (multi-group-by :tags [{:tags ['a 'b], :bar 1}
                             {:tags ['b],    :bar 2}
                             {:tags ['a],    :bar 3}])

  Returns:
      {'a [{:tags ['a 'b], :bar 1}
           {:tags ['a],    :bar 3}]
       'b [{:tags ['a 'b], :bar 1}
           {:tags ['b],    :bar 2}]}"
  [key maps]

  (letfn [(invert [map]
            (into {}
                  (for [val (get map key [])]
                    [val [map]])))]
    (reduce (partial merge-with #(apply conj %1 %2))
            (map invert maps))))

(defn mkdir
  "Create a directory at the given path, creating parent directories as
  necessary."
  [path]
  (.mkdirs (io/file path)))

(defn rmdir-r
  "Remove a directory at the given path, along with everything in it."
  [path]
  ;; Source: this comment by Paul Erdos (@erdos on GitHub):
  ;; https://gist.github.com/edw/5128978#gistcomment-1961491
  (doseq [f (-> path io/file file-seq reverse)]
    (io/delete-file f)))

(defn slugify
  "Convert a string to a URL-friendly slug."
  [s]
  (-> s
      string/lower-case
      (string/replace #"\bof\b" "")
      (string/replace #"\bthe\b" "")
      (string/replace #"\ba\b" "")
      (string/replace #"\ban\b" "")
      (string/replace #"\band\b" "")
      (string/replace #"\bor\b" "")
      (string/replace #"\bwith\b" "")
      (string/replace #"\binto\b" "")
      string/trim
      (string/replace #"\s+" "-")
      (string/replace #"[^a-z0-9-]" "")))

(defn find-files
  "Find files in the given directory, filtering by suffix if one is given. For
  now, only returns files directly contained by the directory and not in
  subdirectories. Returns a seq of path strings."
  ([dir-path] (find-files dir-path nil))
  ([dir-path suffix]
   (let [pred (if (nil? suffix)
                (constantly true)
                #(.endsWith (.getName %) suffix))]
     (->> dir-path
          io/file
          file-seq
          (filter #(not (.isDirectory %)))
          (filter #(not (.isHidden %)))
          (filter pred)
          (mapv #(.getPath %))))))

(defn in-order-dirs
  "Given a seq of file path strings (e.g. from find-files), return a seq of the
  directories that would need to be created if such file paths were to be
  copied elsewhere, such that parent directories appear before child
  directories."
  [paths]
  (let [path->subdirs (fn [path]
                        (let [parts (string/split path #"/")]
                          (for [n (range 1 (count parts))]
                            (string/join "/" (take n parts)))))]
    (->> paths
         (mapcat path->subdirs)
         distinct
         (sort-by #(.length %)))))

(defn needed-dist-dirs
  "Given a seq of paths (e.g. from find-files), presumably all under
  resources/, return a seq of needed directories under dist/.
  Example:
    (needed-dist-dirs [\"resources/foo/bar/baz.txt\"])
    ; => [\"dist/foo\" \"dist/foo/bar\"]"
  [paths]
  (mapv #(string/replace % #"^.*?/" "dist/") (rest (in-order-dirs paths))))
